import { Router } from "express";
import SchedulesController from "../controllers/SchedulesController";

const scheduleRoutes = Router();

scheduleRoutes.post(
  "/create/:professional_id/:user_id",
  SchedulesController.create,
);
scheduleRoutes.get("/list/:professional_id", SchedulesController.create);

export default scheduleRoutes;
