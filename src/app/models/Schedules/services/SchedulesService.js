import Schedules from "../entities/Schedules";

class SchedulesService {
  async list({ professional_id }) {
    const schedules = await Schedules.findAll({ where: { professional_id } });

    if (!schedules) {
      throw new Error("Não existem agendamentos para esse profissional.");
    }

    return schedules;
  }

  async execute({
    data_inicio,
    data_fim,
    hora_inicio,
    hora_fim,
    professional_id,
    user_id,
  }) {
    const existeAppointment = await Schedules.findOne({
      where: { professional_id, data_inicio, hora_inicio },
    });

    if (existeAppointment) {
      throw new Error("Horario indisponivel");
    }

    const appointment = await Schedules.create({
      data_inicio,
      data_fim,
      hora_inicio,
      hora_fim,
      professional_id,
      user_id,
    });

    return appointment;
  }
}

export default new SchedulesService();
