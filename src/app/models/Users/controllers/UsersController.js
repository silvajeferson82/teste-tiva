import UserService from "../services/UserService";

class UsersController {
  async index(_, res) {
    try {
      const users = await UserService.list();

      return res.json(users);
    } catch (err) {
      return res.status(500).json({ Error: err.message });
    }
  }

  async store(req, res) {
    const { name, email, contato, admin, password } = req.body;

    try {
      const user = await UserService.execute({
        name,
        email,
        contato,
        admin,
        password,
      });

      return res.json(user);
    } catch (err) {
      return res.status(500).json({ Error: err.message });
    }
  }

  async update(req, res) {
    const { id } = req.params;
    const { name, email, contato, admin } = req.body;
    try {
      const user = UserService.updateUser({ id, name, email, contato, admin });
      return res.json(user);
    } catch (error) {
      return res.status(500).json({ Error: error.message });
    }
  }
}

export default new UsersController();
