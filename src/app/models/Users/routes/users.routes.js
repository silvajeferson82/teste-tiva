import { Router } from "express";
// eslint-disable-next-line import/extensions
import UsersControllers from "../controllers/UsersController";
import authMiddleware from "../../../../middlewares/auth";

const usersRouter = Router();

usersRouter.get("/list", UsersControllers.index);

usersRouter.use(authMiddleware);
usersRouter.post("/create", UsersControllers.store);
usersRouter.put("/update/:id", UsersControllers.update);

export default usersRouter;
