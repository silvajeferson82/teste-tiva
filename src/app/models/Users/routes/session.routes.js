import { Router } from "express";
import AuthenticateUserController from "../controllers/AuthenticateUserController";

const sessionRouter = Router();

sessionRouter.post("/", AuthenticateUserController.handle);

export default sessionRouter;
