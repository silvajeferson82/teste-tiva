# Tiva Agenda Test

Desafio para a posição de BackEnd JR.

## Instalation

Instalar dependencias

```bash
yarn
```

Conectar ao ElephantSQL database

```bash
DB_HOST=batyr.db.elephantsql.com
DB_USER=cothfkbe
DB_PASSWORD=b5S9b6q5plMOXcy2QbGvrglLY3SdnpZh
DB_NAME=cothfkbe
```

Rodar migrations

```bash
yarn sequelize db:migrate
```

Você pode usar Postbird ou Dbeaver mara manipular as tabelas.

iniciar o serviço

```bash
yarn dev
```

Usuario admin inicial

```bash
{
	"email":"userMaster@email.com",
	"password":"12345"
}
```

Após login sera gerado tokem de acesso.

# Insomnia tests

Use Insomnia as a tester. Just click the button below and it will create the correct environment

[![Run in Insomnia}](https://insomnia.rest/images/run.svg)](https://insomnia.rest/run/?label=teste-tiva-app&uri=https%3A%2F%2Fraw.githubusercontent.com%2Fcvbordalo%2Ftiva-teste-2%2Fmain%2Finsomniaexport.json)
